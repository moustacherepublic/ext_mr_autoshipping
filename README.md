Installation Instructions
=========================

1. Install modman from https://github.com/colinmollenhour/modman
2. Switch to Magento root folder
3. `modman init`
4. `modman clone git@bitbucket.org:moustacherepublic/ext_mr_autoshipping.git`
